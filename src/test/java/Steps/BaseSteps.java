package Steps;

import Base.BaseTest;
import com.thoughtworks.gauge.Step;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class BaseSteps extends BaseTest {

    public WebElement findElement(String key) {
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        WebElement webElement = webDriverWait
                .until(ExpectedConditions.presenceOfElementLocated(infoParam));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                webElement);
        return webElement;
    }

    public List<WebElement> findElements(String key) {
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        return driver.findElements(infoParam);
    }

    @Step("<key> elementi var mi kontrol et")
    public void elementVarKontrol(String key) {
        try {
            findElement(key);
        } catch (Exception e) {
            Assert.fail("Element bulunamadi");
        }
    }

    @Step("<key> elementi bos mu kontrol et")
    public void elementYokKontrol(String key) {
        if (findElements(key).isEmpty()) {
            System.out.println("Liste bos, devam ediliyor");
        } else {
            Assert.fail("Liste dolu");
        }
    }

    @Step("<title> title mi kontrol et")
    public void titleKontrol(String title) {
        Assert.assertEquals(title, driver.getTitle());
        System.out.println("Sayfa yuklendi");
    }

    @Step("<elementKey> elementine <toDoItem> inputunu yaz")
    public void toDoYaz(String elementKey, String toDoItem) {
        findElement(elementKey).sendKeys(toDoItem);
        System.out.println("Yeni item yazildi");
    }

    @Step("<elementKey> elementinde enter tusuna bas")
    public void enterTusunaBas(String elementKey) {
        findElement(elementKey).sendKeys(Keys.ENTER);
    }

    @Step("Listede <itemTitle> itemi var mi kontrol et")
    public void itemKontrol(String itemTitle) {
        List<WebElement> listItems = findElements("listItem");
        System.out.println("Toplam " + listItems.size() + " liste itemi kontrol ediliyor");
        for (int i = 0; i < listItems.size(); i++) {
            System.out.println("Beklenen: " + itemTitle + " listede olan: " + listItems.get(i).getText());
            if (listItems.get(i).getText().equals(itemTitle)) {
                System.out.println("Item bulundu");
                return;
            }
        }
        Assert.fail("Element yok");
        return;
    }

    @Step("<bottomItem> itemi <topItem> iteminden sonra mi kontrol et")
    public void altAltaKontrol(String bottomItem, String topItem) {
        List<WebElement> listItems = findElements("listItem");
        for (int i = 0; i < listItems.size(); i++) {
            if (listItems.get(i).getText().equals(topItem) && listItems.get(i + 1).getText().equals(bottomItem)) {
                System.out.println("Itemler arka arkaya:\n" + topItem + " liste sirasi:" + (i + 1) + "\n" + bottomItem + " liste sirasi:" + (i + 1));
                return;
            }
        }
        Assert.fail("Itemler alt alta degil");
    }

    @Step("Listede marksiz <itemTitle> iteminin radio butonuna tikla")
    public void radioTikla(String itemTitle) {
        List<WebElement> radioButtons = findElements("unmarkedListItemRadio");
        List<WebElement> listItems = findElements("unmarkedListItem");

        for (int i = 0; i < listItems.size(); i++) {
            if (listItems.get(i).getText().equals(itemTitle)) {
                action.moveToElement(radioButtons.get(i)).click().build().perform();
                System.out.println(listItems.get(i).getText() + " elementi tiklendi");
                return;
            }
        }
        Assert.fail("Element yok");
    }

    @Step("Listede markli <itemTitle> iteminin radio butonuna tikla")
    public void radioMarkliTikla(String itemTitle) {
        List<WebElement> radioButtons = findElements("markedListItemRadio");
        List<WebElement> listItems = findElements("markedListItem");

        for (int i = 0; i < listItems.size(); i++) {
            if (listItems.get(i).getText().equals(itemTitle)) {
                action.moveToElement(radioButtons.get(i)).click().build().perform();
                System.out.println(listItems.get(i).getText() + " elementinin tiki kaldirildi");
                return;
            }
        }
        Assert.fail("Element yok");
    }

    @Step("Listede <itemTitle> itemi radio butonu tikli mi kontrol et")
    public void radioTikKontrol(String itemTitle) {
        List<WebElement> listItems = findElements("markedListItem");

        for (int i = 0; i < listItems.size(); i++) {
            if (listItems.get(i).getText().equals(itemTitle)) {
                System.out.println(listItems.get(i).getText() + " elementi tikli, kontrol edildi");
                return;
            }
        }
        Assert.fail("Element yok");
    }

    @Step("Listede <itemTitle> itemi radio butonu tiksiz mi kontrol et")
    public void radioTiksizKontrol(String itemTitle) {
        List<WebElement> listItems = findElements("unmarkedListItem");

        for (int i = 0; i < listItems.size(); i++) {
            if (listItems.get(i).getText().equals(itemTitle)) {
                System.out.println(listItems.get(i).getText() + " elementi tiksiz, kontrol edildi");
                return;
            }
        }
        Assert.fail("Element yok");
    }

    @Step("Listede <itemTitle> iteminin silme butonuna tikla")
    public void silmeTikla(String itemTitle) {
        List<WebElement> listItems = findElements("listItem");
        List<WebElement> deleteButtons = findElements("listItemDeleteButton");

        for (int i = 0; i < listItems.size(); i++) {
            if (listItems.get(i).getText().equals(itemTitle)) {
                System.out.println(listItems.get(i).getText() + " elementi bulundu, siliniyor");
                deleteButtons.get(i).click();
                return;
            }
        }
        Assert.fail("Element yok");
    }

    @Step("Listede <itemTitle> itemi yok mu kontrol et")
    public void itemYokKontrol(String itemTitle) {
        List<WebElement> listItems = findElements("listItem");
        System.out.println("Toplam " + listItems.size() + " liste itemi kontrol ediliyor");
        for (int i = 0; i < listItems.size(); i++) {
            System.out.println("Beklenen: " + itemTitle + " listede olan: " + listItems.get(i).getText());
            if (listItems.get(i).getText().equals(itemTitle)) {
                Assert.fail("Item bulundu");
                return;
            }
        }
        System.out.println("Item yok, kontrol edildi");
        return;
    }
}
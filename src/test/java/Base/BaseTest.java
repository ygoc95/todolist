package Base;

import com.thoughtworks.gauge.AfterSpec;
import com.thoughtworks.gauge.BeforeSpec;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class BaseTest {

    public static WebDriver driver;
    public static Actions action;
    static String url = "https://todomvc.com/examples/vue/";

    @BeforeSpec
    public void senaryoBasladi(){
        System.out.println("----- Senaryo Basladi -----");
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        action = new Actions(driver);
        driver.manage().window().maximize();
        driver.get(url);
    }

    @AfterSpec
    public void senaryoSonlandi(){
        driver.quit();
        System.out.println("----- Senaryo Sonlandi -----");
    }
}
To Do List Senaryo
==================
Tags:toDoList


Bos listeye item eklenmesi ve kontrolu
------------------
*Sayfa yuklendi mi kontrol et
*Liste bos mu kontrol et
*"Proje Yapilacak" itemi ekle
*Listede "Proje Yapilacak" itemi var mi kontrol et

Bos olmayan listeye item eklenmesi ve siralama kontrolu
------------------
*Sayfa yuklendi mi kontrol et
*Listede "Proje Yapilacak" itemi var mi kontrol et
*"Bilgisayari al" itemi ekle
*Listede "Bilgisayari al" itemi "Proje Yapilacak" itemi altinda mi kontrol et

Bos olmayan listede marklama islemi ve kontrolu
------------------
*Sayfa yuklendi mi kontrol et
*Listede "Proje Yapilacak" itemi var mi kontrol et
*Listede "Bilgisayari al" itemi var mi kontrol et
*Listede marksiz "Proje Yapilacak" itemini markla
*Listede "Proje Yapilacak" itemi markli mi kontrol et

Bos olmayan listede mark kaldirma islemi ve kontrolu
------------------
*Sayfa yuklendi mi kontrol et
*Listede markli var mi kontrol et
*Listede markli "Proje Yapilacak" itemini markla
*Listede "Proje Yapilacak" itemi marksiz mi kontrol et

Bos olmayan listede item ekleme ve eski item silme islemi ve kontrolu
------------------
*Sayfa yuklendi mi kontrol et
*Listede "Proje Yapilacak" itemi var mi kontrol et
*Listede "Bilgisayari al" itemi var mi kontrol et
*"Herseyi duzenli yap" itemi ekle
*Listede "Herseyi duzenli yap" itemi var mi kontrol et
*Listede "Proje Yapilacak" itemini sil
*Listede "Proje Yapilacak" itemi silinmis mi kontrol et